#
# You should set the INCLUDED_MAKE_PATH variable to wherever you have
# downloaded the templates module.
#
TOPDIR=../../..
TARGET=slides.pdf 
DVI=$(TARGET:.pdf=.dvi)
SRC=$(TARGET:.pdf=.tex)
COMMONSOURCES=conclusioni.tex domani.tex emergenza_del_timbro.tex musica_elettronica.tex\
							nuove_concezioni_orchestrali.tex oggi.tex spettralismo.tex varese.tex
AUX=$(TARGET):.pdf=.aux)
CURDIRNAME=TCMC
IMAGEDIRNAME=images
IMAGEDIR=$(TOPDIR)/$(IMAGEDIRNAME)
EXAMPLEDIRNAME=examples
EXAMPLEDIR=$(TOPDIR)/$(EXAMPLEDIRNAME)
BEAMERDIR=$(TOPDIR)/../latex-templates/beamer/smerm
COMMONDIR=../common
VPATH=.:$(COMMONDIR)
TEXINPUTS=".:$(BEAMERDIR):$(IMAGEDIR):"
PDFLATEX=TEXINPUTS=$(TEXINPUTS) pdflatex
PDFLATEXINPUT='\newcommand{\printmode}{$(PRINTMODE)}\input{$<}'
SLIDESVERSION!=git show --oneline | head -1 | cut -d ' ' -f 1
SLIDESDATE!=date +%Y%m%d
DISTNAME_WO_SFX=CMTSA-Bernardini-$(SLIDESVERSION)-$(SLIDESDATE)
DISTNAME=$(DISTNAME_WO_SFX).pdf
DATADIR=.
COPY=cp --preserve
MOVE=mv
BIBTEX=bibtex
LN=ln -sf
CD=cd
MV=mv
ECHO=/bin/echo -e
MAKEFLAGS+=-w
ZIP=zip -r
METAFILES=.slidesversion .slidesdate
DISTFILE=$(DISTNAME_WO_SFX).zip
PRINTMODE=beamer


all: $(TARGET) #examples images

dist: $(DISTNAME)

$(TARGET): $(COMMONSOURCES) $(SRC)

$(DISTNAME): $(TARGET)
	$(COPY) $(TARGET) $@
	
%.pdf:	%.tex
	$(PDFLATEX) $(PDFLATEXINPUT)
	$(RM) $@
	$(PDFLATEX) $(PDFLATEXINPUT)

images:
	$(MAKE) -C $(IMAGEDIR) -$(MAKEFLAGS)

examples:
	$(MAKE) -C $(EXAMPLEDIR) -$(MAKEFLAGS)

distclean:
	$(RM) *~ *.aux *.dvi *.out *.log *.bbl *.blg *.toc *.nav *.snm *.pdf $(METAFILES)

clean: distclean

.slidesversion: $(TARGET)
	git show --oneline | head -1 | cut -d ' ' -f 1 > $@

.slidesdate: $(TARGET)
	date +%Y%m%d > $@

.PHONY:	distclean images realtarget $(CUR_LECTURE)

.SUFFIXES:	.pdf .ps .dvi .aux .tex
