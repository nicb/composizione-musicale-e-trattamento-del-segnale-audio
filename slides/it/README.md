# Annotazioni per le slides


* Introduzione storica:

  * Emergenza del timbro nella musica d'autore
	* Varèse e gli accordi di risonanza
	* La nascita della musica elettronica
  * Le    nuove     concezioni     orchestrali     (Maderna/Berio:
    l'orchestrazione per "famiglie  acustiche")  esempi:  Requies,
    Formazioni, un esempio di Maderna (forse Quadrivium?)

* Lo spettralismo:

  * Principi compositivi => analisi dei segnali musicali
	* Soluzioni armoniche
	* Varianti => Grisey, Murail, Dufour, Manoury
	* Esempi analitici: prime battute di Modulations (Grisey),
    altro esempio? (forse  qualche  battuta  dei  13  couleurs  du
    soleil couchant di Murail)

* Oggi:

  * tecniche granulari + statistica: THEMA Vaggione, Recordare
	* lenti deformanti spettrali: Studi per pianoforte
	* spazializzazione: WFS, HOA

* Domani:

  * SMS tools and alike
  * croma e indicizzazione della dissonanza
	* tecniche di Music Information Retrieval usate a fini compositivi

* Conclusioni:

  * il rapporto tra composizione e ricerca tecnologica è biunivoco
	  (talvolta i compositori prendono spunto, talvolta "spingono" la ricerca)
	* rapporto proficuo di ricerca tra compositori e ingegneri
