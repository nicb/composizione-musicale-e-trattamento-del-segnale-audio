
         ＣＯＭＰＯＳＩＺＩＯＮＥ  ＭＵＳＩＣＡＬＥ  Ｅ
    ＴＲＡＴＴＡＭＥＮＴＯ  ＤＥＬ  ＳＥＧＮＡＬＥ  ＡＵＤＩＯ

                      Nicola Bernardini
                     con  Anna Terzaroli
                         (c) 2015


Negli ultimi quarant'anni, il trattamento del segnale audio  è  entrato
prepotentemente nella composizione musicale  contemporanea.  L'uso  più
noto è quello delle cosiddette musiche elettroacustiche ed acusmatiche.
Tuttavia, le stesse tecniche compositive hanno recepito  le  suggestioni
proposte dall'elaborazione numerica dei suoni e ne hanno  interiorizzato
le innovazioni.

Questo seminario intende tratteggoare a grandi linee questo fenomeno  di
cross-breeding multidisciplinare per  illustrare  alcuni  utilizzi  non-
convenzionali dell'analisi e della risintesi del segnale audio da  parte
di diverse generazioni di  compositori.  A  partire  dall'esperienza  di
Luciano Berio prima, Gérard Grisey e Tristan Murail in seguito sino  ad
alcune  tecniche  sviluppate  ad  hoc   dall'autore   per   le   proprie
composizioni, si verrà delineando un percorso del pensiero musicale nel
quale il  trattamento  del  segnale  audio  è  diventato  completamente
organico.
