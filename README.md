# Composizione Musicale e Trattamento del Segnale Audio

"Composizione Musicale e Trattamento del Segnale Audio" is a lecture on
contemporary methods for musical composition related to signal
processing (in italian, currently).

## Compiled version of the slides

A compiled version of the slides may be found over to
[SlideShare](http://www.slideshare.net/NicolaBernardini2/composizione-musicale-e-trattamento-del-segnale-audio)

## Compilation Instructions

In order to compile the slides you need to also clone the [LaTeX
templates](https://github.com/nicb/latex-templates) at the same level of the
this top root directory.

## Presentation log

* Sapienza Università di Roma, Facoltà di Ingegneria, Master *Trattamento del Segnale Audio*, Roma October 28 2015
